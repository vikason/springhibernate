package com.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author vikas
 */
@Entity
@Table(name = "student")
public class StudentEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "rollno")
    private Long rollNo;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    public StudentEntity() {
    }

    public Long getRollNo() {
        return rollNo;
    }

    public void setRollNo(Long rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public StudentEntity(Long rollNo, String name, String address) {
        this.rollNo = rollNo;
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "StudentEntity{" + "rollNo=" + rollNo + ", name=" + name + ", address=" + address + '}';
    }
}
