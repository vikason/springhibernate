package com.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author vikas
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "StudentEntity.studentList",
            query = "from StudentEntity s"
    ),
    @NamedQuery(
            name = "StudentEntity.getStudent",
            query = "from StudentEntity s where s.rollNo  = :rollno"
    ),
    @NamedQuery(
            name = "StudentEntity.update",
            query = "update StudentEntity s set s.name = :name, s.address = :address "
            + "where s.rollNo  = :rollno"
    )
})
public class NamedQueryMaster implements Serializable {

    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
