
package com.dao;

import com.entity.StudentEntity;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author vikas
 */
@Controller("StudentOperationDao")
public class StudentOperationDaoImpl implements StudentOperationDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session session;
    private Transaction transaction;

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(StudentEntity entity) {
        session = getSession();
        transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.flush();
    }

    @Override
    public StudentEntity searchStudent(long key) {
        session = getSession();
        Query q = session.getNamedQuery("StudentEntity.studentList");
        return (StudentEntity) q.list().get(0);

    }

    private Session getSession() {
        return sessionFactory.openSession();
    }

    @Override
    public List<StudentEntity> getStudentList() {

        session = getSession();
        Query q = session.getNamedQuery("StudentEntity.studentList");
        return q.list();
    }

    @Override
    public void update(StudentEntity entity) {
        session = getSession();
        transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
    }

    @Override
    public void delete(StudentEntity entity) {
        session = getSession();
        transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();

    }
}
