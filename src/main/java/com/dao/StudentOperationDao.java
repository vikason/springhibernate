package com.dao;

import com.entity.StudentEntity;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 *
 * @author vikas
 */
public interface StudentOperationDao {

    void addUser(StudentEntity entity);

    SessionFactory getSessionFactory();

    void setSessionFactory(SessionFactory sessionFactory);

    StudentEntity searchStudent(long key);

    public List<StudentEntity> getStudentList();

    public void update(StudentEntity entity);

    public void delete(StudentEntity entity);
}
