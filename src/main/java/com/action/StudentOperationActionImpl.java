package com.action;

import com.dao.StudentOperationDao;
import com.entity.StudentEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author vikas
 */
@Controller("StudentOperationAction")
public class StudentOperationActionImpl
        implements StudentOperationAction {

    @Autowired
    private StudentOperationDao studentOperationDao;

    @Override
    public void addUser(StudentEntity entity) {

        studentOperationDao.addUser(entity);
    }

    @Override
    public void deleteUser(long rollNo) {

    }

    @Override
    public StudentEntity searchUser(long rollNo) {
        return studentOperationDao.searchStudent(rollNo);
    }

    public StudentOperationDao getStudentOperationDao() {
        return studentOperationDao;
    }

    public void setStudentOperationDao(StudentOperationDao studentOperationDao) {
        this.studentOperationDao = studentOperationDao;
    }

    @Override
    public List<StudentEntity> getStudentList() {

        return studentOperationDao.getStudentList();
    }

    @Override
    public void update(StudentEntity entity) {
        studentOperationDao.update(entity);
    }

    @Override
    public void deleteStudent(StudentEntity entity) {
        studentOperationDao.delete(entity);
    }

}
