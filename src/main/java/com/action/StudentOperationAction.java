
package com.action;

import com.entity.StudentEntity;
import java.util.List;

/**
 *
 * @author vikas
 */
public interface StudentOperationAction {

    public void addUser(StudentEntity entity);

 
    public void deleteUser(long rollNo);

    public StudentEntity searchUser(long rollNo) ;
    
    public List<StudentEntity> getStudentList();

    public void update(StudentEntity entity);

    public void deleteStudent(StudentEntity entity);

}
