
package com;

import com.action.StudentOperationAction;
import com.entity.StudentEntity;
import java.util.List;
import java.util.Scanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author vikas
 */
public class SpringHibernate {

    private static final String SPRING_CONFIG = "spring.xml";
    private static final String ACTION_CLASS_NAME = "StudentOperationAction";
    private static final String LIST_ALL = "1";
    private static final String ADD_STUDENT = "2";
    private static final String UPDATE = "3";
    private static final String DELETE = "4";
    private static final String SEARCH = "5";
    private static final String EXIT = "e";
    private static final String LOGOUT = "e";
    private static ApplicationContext context;
    private static StudentOperationAction action;

    public static void main(String[] a) {

        context = new ClassPathXmlApplicationContext(SPRING_CONFIG);

        action = (StudentOperationAction) context.getBean(ACTION_CLASS_NAME);

        String choice = null;
        while (true) {
            if (choice != null && choice.equalsIgnoreCase(LOGOUT)) {
                break;
            }
            choice = showMenu();
            switch (choice) {

                case LIST_ALL:
                    doListAll();
                    break;
                case ADD_STUDENT:
                    doAddStudent();
                    break;
                case UPDATE:
                    doUpdate();
                    break;
                case DELETE:
                    doDeleteUser();
                    break;

                case SEARCH:
                    getStudent();
                    break;

                case EXIT:
                    break;
                default:
                    System.out.println("Invalid options selected...");
                    break;
            }
        }

    }

    private static String showMenu() {
        Scanner s = new Scanner(System.in);
        System.out.println("Welcome ");
        System.out.println("");
        System.out.println(LIST_ALL + ": List all Student");
        System.out.println(ADD_STUDENT + ": Add Student");
        System.out.println(UPDATE + ": Update Student");
        System.out.println(DELETE + ": Delect Student");
        System.out.println(SEARCH + ": Change Password");

        System.out.println(EXIT + ": EXIT");
        System.out.print("Input your Choice: ");
        String choice = s.nextLine();
        return choice;
    }

    private static void doListAll() {
        /**
         * Search all students
         */
        List<StudentEntity> list = action.getStudentList();
        for (StudentEntity se : list) {
            System.out.println(se.toString());
        }
    }

    private static void doAddStudent() {
        /**
         * add operation
         */
        Scanner s = new Scanner(System.in);
        StudentEntity entity = new StudentEntity();
        System.out.print("Roll No.: ");
        entity.setRollNo(Long.parseLong(s.nextLine()));

        System.out.print("Name: ");
        entity.setName(s.nextLine());

        System.out.print("Address: ");
        entity.setAddress(s.nextLine());

        action.addUser(entity);
    }

    private static void doUpdate() {
        /**
         * Update User
         */

        Scanner s = new Scanner(System.in);
        StudentEntity entity = new StudentEntity();
        System.out.print("Roll No.: ");
        entity.setRollNo(Long.parseLong(s.nextLine()));

        System.out.print("Name: ");
        entity.setName(s.nextLine());

        System.out.print("Address: ");
        entity.setAddress(s.nextLine());

        action.update(entity);
    }

    private static void getStudent() {
        /**
         * search specific student operation
         */
        Scanner s = new Scanner(System.in);
        System.out.print("Roll No.: ");
        Long rollNo = Long.parseLong(s.nextLine());

        StudentEntity entity;
        entity = action.searchUser(rollNo);
        System.out.println(entity.toString());

    }

    private static void doDeleteUser() {
        /**
         * delete student operation
         */
        Scanner s = new Scanner(System.in);
        System.out.print("Roll No.: ");
        Long rollNo = Long.parseLong(s.nextLine());

        StudentEntity entity;
        entity = action.searchUser(rollNo);
        action.deleteStudent(entity);
    }
}
